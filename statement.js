/**
 *  Функция генерации данных для счета по заказу
 *  *Входные данные
 *
 *  Заказ
 *  invoice{
 *      customer: string,
 *      performance: [
 *          {
 *              playId: number,
 *              audience: number
 *          } Object
 *      ] Array
 *  } Object
 *
 *  Пьессы
 *  plays[
 *      {
 *          name: string,
 *          type: string
 *      }Object
 *  ]Array
 *
 *  *Выходные данные
 *  result{
 *      customer:string,
 *      performance:[
 *          {
 *              name: string,
 *              amount: number,
 *              audience: number
 *          }Object
 *      ]Array,
 *      totalAmount:number,
 *      volumeCredits:number
 *  }Object
 * */
module.exports.statement = (invoice, plays)=>{
    let result = {
        customer:invoice.customer, // заказчик
        performance:[], // Массив пьесс
        totalAmount:0, // Общая сумма
        volumeCredits:0 // Бонусы
    };
    //Перебор пьесс
    invoice.performance.map( perf => {
        const play = plays[perf.playId];
        if(play === undefined) throw new Error(`неизвестный id пьесы: ${perf.playId}`);
        let thisAmount = 0;
        switch (play.type) {
            case "tragedy":
                thisAmount = 40000;
                if (perf.audience > 30) {
                    thisAmount += 1000 * (perf.audience - 30);
                }
                break;
            case "comedy":
                thisAmount = 30000;
                if (perf.audience > 20) {
                    thisAmount += 10000 + 500 * (perf.audience - 20);
                }
                thisAmount += 300 * perf.audience;
                break;
            default:
                throw new Error(`неизвестный тип: ${play.type}`);
        }
        // Добавление бонусов
        result.volumeCredits += Math.max(perf.audience - 30, 0);
        // Дополнительный бонус за каждые 10 посетителей комедий
        if ("comedy" === play.type) result.volumeCredits += Math.floor(perf.audience / 10);
        // добавление счета в список
        result.performance.push({
            name:play.name,
            amount:thisAmount,
            audience: perf.audience
        });
        // Увеличение общей суммы за заказ
        result.totalAmount += thisAmount;
    });
    return result;
};