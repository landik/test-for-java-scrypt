const testModule = require('./statement.js');
const fs = require('fs');
const invoices = JSON.parse(fs.readFileSync('./invoices.json').toString());
const plays = JSON.parse(fs.readFileSync('./plays.json').toString());
invoices.map(invoice => {
    try{
        let result = testModule.statement(invoice,plays);
        console.log(result);
    }catch(e){
        console.log(e)
    }
})